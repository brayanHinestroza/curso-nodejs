var Bicicleta = require('../models/bicicleta')

exports.bicicleta_list = function (req,res) {
    res.render('bicicleta/index', {bicis: Bicicleta.allBicis});
}

exports.bicicleta_create_get = function (req,res) {
    res.render('bicicleta/create');
}

exports.bicicleta_create_post = function (req,res) {
    const {id, color, modelo, lat, long} = req.body;
    var bici = new Bicicleta(id, color, modelo, [lat, long]);
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function (req,res) {
    Bicicleta.removeById(req.body.id)
    res.redirect('/bicicletas')
}

exports.bicicleta_update_get = function (req,res) {
    var bici = Bicicleta.findById(req.params.id)
    res.render('bicicleta/update', {bici});
}

exports.bicicleta_update_post = function (req,res) {
    const {id, color, modelo, lat, long} = req.body;
    var bici = Bicicleta.findById(req.params.id)
    bici.id = id
    bici.color = color
    bici.modelo = modelo
    bici.ubicacion = [lat, long];

    res.redirect('/bicicletas');
}