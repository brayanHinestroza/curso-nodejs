var Bicicleta = require('../../models/bicicleta')


exports.bicicleta_list = function (req,res) {
    res.status(200).json({
        Bicicleta: Bicicleta.allBicis
    });
}

exports.bicicleta_create = function (req,res) {
    const {id, color, modelo, lat, long} = req.body
    var bici = new Bicicleta(id, color, modelo, [lat, long]);

    Bicicleta.add(bici);
    res.status(200).json({
        Bicicleta:bici
    });
}

exports.bicicleta_remove = function (req,res) {
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}

