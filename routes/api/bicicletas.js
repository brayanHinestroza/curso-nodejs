var express = require('express')
var router = express.Router();
var BicicletaController = require('../../controllers/api/bicicletaControllerAPI')

router.get('/bicicletas', BicicletaController.bicicleta_list);
router.post('/create', BicicletaController.bicicleta_create);
router.post('/delete', BicicletaController.bicicleta_remove);

module.exports = router;